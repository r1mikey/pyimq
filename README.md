[Python](http://www.python.org/) interface to [Open Message Queue](http://mq.java.net/) (also known as the Glassfish Message Queue, formerly known as the Sun Java System Message Queue and informally known as IMQ).

This software wraps the [Sun GlassFish Message Queue C runtime library](http://docs.oracle.com/cd/E19879-01/821-0030/index.html) (mqcrt or libmqcrt) in Python, modelling the implied object oriented design of the C interface.

Please see the BUILDING files for build instructions for your platform.
