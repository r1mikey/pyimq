from distutils.core import setup, Extension

INCLUDES = []
LIBDIRS = []
RUNDIRS = []
LIBS = []
#INCLUDES = ["/Users/michael/sw/include/mqcrt", "/Users/michael/sw/include/nss", "/Users/michael/sw/include/nspr"]
#LIBDIRS = ["/Users/michael/sw/lib"]
#RUNDIRS = ["/Users/michael/sw/lib"]
#LIBS = ['mqcrt', 'ssl3', 'nss3', 'plc4', 'plds4', 'nspr4', 'softokn3']

extension = Extension(
    name = "imq",
    include_dirs = INCLUDES,
    library_dirs = LIBDIRS,
    runtime_library_dirs = RUNDIRS,
    libraries = LIBS,
    sources = ["src/imq.c"],
    depends = [
        "src/imq.h",
        "src/mqconnection-callbacks.c",
        "src/mqconnection.c",
        "src/mqconsumer-callbacks.c",
        "src/mqconsumer.c",
        "src/mqdestination.c",
        "src/mqmessage.c",
        "src/mqproducer.c",
        "src/mqsession.c",
        "src/mqstatus.c",
        "src/ssl.c",
        "src/util.c"
    ],
)

setup(
    name = "pyimq",
    version = "0.0.5",
    description = "Python interface to the Glassfish Message Queue",
    author = "Michael van der Westhuizen",
    author_email = "r1mikey@gmail.com",
    license = "BSD",
    url = "https://bitbucket.org/r1mikey/pyimq",
    ext_modules = [extension]
)
