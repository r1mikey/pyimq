/*
 * Copyright (c) 2009, Michael van der Westhuizen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    - Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    - Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef IMQ_SSL_C_MCVDW_20090517
#define IMQ_SSL_C_MCVDW_20090517

#include "util.c"



static PyObject * mqcrt_MQInitializeSSL(PyObject *self, PyObject *args, PyObject *kwds)
{
    PyObject *certdb_path;
    static char *kwlist[] = {"certdb_path", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "O", kwlist, &certdb_path)) {
        return NULL;
    }

    if (NULL == (certdb_path = coerceObjectToUtf8String(certdb_path))) {
        return NULL;
    }

    if (setExceptionFromStatus(MQInitializeSSL(UTF8_STRING_TO_CHAR_ARRAY(certdb_path)))) {
        Py_CLEAR(certdb_path);
        return NULL;
    }

    Py_CLEAR(certdb_path);
    Py_RETURN_NONE;
}

#endif
