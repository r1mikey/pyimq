/*
 * Copyright (c) 2009, Michael van der Westhuizen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    - Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    - Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MQPRODUCER_MCVDW_20090517_C
#define MQPRODUCER_MCVDW_20090517_C

#include "imq.h"

#include "mqstatus.c"
#include "util.c"
#include "mqdestination.c"
#include "mqmessage.c"



static int mqcrt_MQProducer_close_producer(mqcrt_MQProducer *self, int raise_errors)
{
    MQStatus status;
    MQProducerHandle handle;

    DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_close_producer: entry"));

    if(self->closed) {
        DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_close_producer: exit [already closed]"));
        return 1;
    }

    COPY_HANDLE(handle, self->producer);
    INIT_HANDLE(self->producer);
    self->closed = 1;

    if (HANDLE_IS_VALID(handle)) {
        status = MQCloseMessageProducer(handle);

        if(raise_errors) {
            if (setExceptionFromStatus(status)) {
                self->closed = 0;
                COPY_HANDLE(self->producer, handle);
                DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_close_producer: exit [error closing handle]"));
                return 0;
            }
        } else {
            if (MQStatusIsError(status)) {
                MQString emsg;

                if (NULL != (emsg = MQGetStatusString(status))) {
                    DPRINTF(("INFO: mqcrt_MQProducer_close_producer: problem closing handle [error code %u: %s]\n", MQGetStatusCode(status), emsg));
                    MQFreeString(emsg);
                } else {
                    DPRINTF(("INFO: mqcrt_MQProducer_close_producer: problem closing handle [error code %u]\n", MQGetStatusCode(status)));
                }
            }
        }
    }

    DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_close_producer: exit"));
    return 1;
}


static int mqcrt_MQProducer_traverse(mqcrt_MQProducer *self, visitproc visit, void *arg)
{
    DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_traverse: entry"));
    Py_VISIT(self->session);
    DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_traverse: exit"));
    return 0;
}


static int mqcrt_MQProducer_clear(mqcrt_MQProducer *self)
{
    DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_clear: entry"));
    Py_CLEAR(self->session);
    DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_clear: exit"));
    return 0;
}


static void mqcrt_MQProducer_dealloc(mqcrt_MQProducer *self)
{
    DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_dealloc: entry"));
    mqcrt_MQProducer_close_producer(self, 0);
    mqcrt_MQProducer_clear(self);
    Py_TYPE(self)->tp_free((PyObject*)self);
    DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_dealloc: exit"));
}


static PyObject * mqcrt_MQProducer_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    mqcrt_MQProducer *self;
    DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_new: entry"));

    if (NULL == (self = (mqcrt_MQProducer *)type->tp_alloc(type, 0))) {
        return NULL;
    }

    INIT_HANDLE(self->producer);
    self->closed = 1;
    self->session = NULL;
    DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_new: exit"));
    return (PyObject *)self;
}


static PyObject * mqcrt_MQProducer_close(mqcrt_MQProducer *self, PyObject *args, PyObject *kwds)
{
    MQProducerHandle handle;
    MQStatus status;

    if (self->closed) {
        Py_RETURN_NONE;
    }

    COPY_HANDLE(handle, self->producer);

    Py_INCREF(self);
    Py_BEGIN_ALLOW_THREADS
    status = MQCloseMessageProducer(handle);
    Py_END_ALLOW_THREADS
    Py_DECREF(self);

    if (setExceptionFromStatus(status)) {
        return NULL;
    }

    INIT_HANDLE(self->producer);
    self->closed = 1;
    Py_RETURN_NONE;
}


static PyObject * mqcrt_MQProducer_send(mqcrt_MQProducer *self, PyObject *args, PyObject *kwds)
{
    MQStatus status;
    PyObject * msg;
    static char *kwlist[] = {"message", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "O", kwlist, &msg)) {
        return NULL;
    }

    Py_INCREF(msg);
    Py_BEGIN_ALLOW_THREADS
    status = MQSendMessage(self->producer, ((mqcrt_MQMessage *)msg)->message);
    Py_END_ALLOW_THREADS
    Py_DECREF(msg);

    if (setExceptionFromStatus(status)) {
        return NULL;
    }

    Py_RETURN_NONE;
}


static PyObject * mqcrt_MQProducer_send_ext(mqcrt_MQProducer *self, PyObject *args, PyObject *kwds)
{
    MQStatus status;
    mqcrt_MQMessage * msg;
    MQDeliveryMode msgDeliveryMode;
    MQInt32 msgPriority; /* XXX: actually MQInt8 -- should be format "b" */
    MQInt64 msgTimeToLive;

    static char *kwlist[] = {"message", "delivery_mode", "priority", "ttl", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "Oiii", kwlist, &msg, &msgDeliveryMode, &msgPriority, &msgTimeToLive)) {
        return NULL;
    }

    Py_INCREF((PyObject *)msg);
    Py_INCREF((PyObject *)self);
    Py_BEGIN_ALLOW_THREADS
    status = MQSendMessageExt(self->producer, msg->message, msgDeliveryMode, msgPriority, msgTimeToLive);
    Py_END_ALLOW_THREADS
    Py_DECREF((PyObject *)self);
    Py_DECREF((PyObject *)msg);

    if (setExceptionFromStatus(status)) {
        return NULL;
    }

    Py_RETURN_NONE;
}


static PyObject * mqcrt_MQProducer_send_to_destination(mqcrt_MQProducer *self, PyObject *args, PyObject *kwds)
{
    MQStatus status;
    mqcrt_MQMessage * msg;
    mqcrt_MQDestination * dst;
    static char *kwlist[] = {"message", "destination", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "OO", kwlist, &msg, &dst)) {
        return NULL;
    }

    Py_INCREF((PyObject *)msg);
    Py_INCREF((PyObject *)dst);
    Py_INCREF((PyObject *)self);
    Py_BEGIN_ALLOW_THREADS
    status = MQSendMessageToDestination(self->producer, msg->message, dst->destination);
    Py_END_ALLOW_THREADS
    Py_DECREF((PyObject *)self);
    Py_DECREF((PyObject *)dst);
    Py_DECREF((PyObject *)msg);

    if (setExceptionFromStatus(status)) {
        return NULL;
    }

    Py_RETURN_NONE;
}


static PyObject * mqcrt_MQProducer_send_to_destination_ext(mqcrt_MQProducer *self, PyObject *args, PyObject *kwds)
{
    MQStatus status;
    mqcrt_MQMessage * msg;
    mqcrt_MQDestination * dst;
    MQDeliveryMode msgDeliveryMode;
    MQInt32 msgPriority; /* actually MQInt8 */
    MQInt64 msgTimeToLive;

    static char *kwlist[] = {"message", "destination", "delivery_mode", "priority", "ttl", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "OOiii", kwlist, &msg, &dst, &msgDeliveryMode, &msgPriority, &msgTimeToLive)) {
        return NULL;
    }

    Py_INCREF((PyObject *)msg);
    Py_INCREF((PyObject *)dst);
    Py_INCREF((PyObject *)self);
    Py_BEGIN_ALLOW_THREADS
    status = MQSendMessageToDestinationExt(self->producer, msg->message, dst->destination, msgDeliveryMode, msgPriority, msgTimeToLive);
    Py_END_ALLOW_THREADS
    Py_DECREF((PyObject *)self);
    Py_DECREF((PyObject *)dst);
    Py_DECREF((PyObject *)msg);

    if (setExceptionFromStatus(status)) {
        return NULL;
    }

    Py_RETURN_NONE;
}


static PyMethodDef mqcrt_MQProducer_methods[] = {
    {"close", (PyCFunction)mqcrt_MQProducer_close, METH_NOARGS, "Close the message producer."},
    {"send", (PyCFunction)mqcrt_MQProducer_send, METH_VARARGS | METH_KEYWORDS, "Send a message. Use only when this producer has been created for a destination."},
    {"send_ext", (PyCFunction)mqcrt_MQProducer_send_ext, METH_VARARGS | METH_KEYWORDS, "Send a message. Use only when this producer has been created for a destination."},
    {"send_to_destination", (PyCFunction)mqcrt_MQProducer_send_to_destination, METH_VARARGS | METH_KEYWORDS, "Send a message to a destination. Use only when this producer has not been created for a destination."},
    {"send_to_destination_ext", (PyCFunction)mqcrt_MQProducer_send_to_destination_ext, METH_VARARGS | METH_KEYWORDS, "Send a message to a destination. Use only when this producer has not been created for a destination."},
    {NULL}
};

static PyTypeObject mqcrt_MQProducerType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "imq.MQProducer",                        /* tp_name */
    sizeof(mqcrt_MQProducer),                /* tp_basicsize */
    0,                                       /* tp_itemsize */
    (destructor)mqcrt_MQProducer_dealloc,    /* tp_dealloc */
    0,                                       /* tp_print */
    0,                                       /* tp_getattr */
    0,                                       /* tp_setattr */
    0,                                       /* tp_compare */
    0,                                       /* tp_repr */
    0,                                       /* tp_as_number */
    0,                                       /* tp_as_sequence */
    0,                                       /* tp_as_mapping */
    0,                                       /* tp_hash */
    0,                                       /* tp_call */
    0,                                       /* tp_str */
    0,                                       /* tp_getattro */
    0,                                       /* tp_setattro */
    0,                                       /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE | Py_TPFLAGS_HAVE_GC, /* tp_flags */
    "MQProducer objects",                    /* tp_doc */
    (traverseproc)mqcrt_MQProducer_traverse, /* tp_traverse */
    (inquiry)mqcrt_MQProducer_clear,         /* tp_clear */
    0,                                       /* tp_richcompare */
    0,                                       /* tp_weaklistoffset */
    0,                                       /* tp_iter */
    0,                                       /* tp_iternext */
    mqcrt_MQProducer_methods,                /* tp_methods */
    0,                                       /* tp_members */
    0,                                       /* tp_getset */
    0,                                       /* tp_base */
    0,                                       /* tp_dict */
    0,                                       /* tp_descr_get */
    0,                                       /* tp_descr_set */
    0,                                       /* tp_dictoffset */
    0,                                       /* tp_init */
    0,                                       /* tp_alloc */
    0,                                       /* tp_new */
    0,
};


static PyObject * mqcrt_MQProducer_Factory(MQProducerHandle handle, mqcrt_MQSession * session)
{
    mqcrt_MQProducer * prod;

    DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_Factory: entry"));

    if (!HANDLE_IS_VALID(handle)) {
        DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_Factory: error [invalid producer handle]"));
        PyErr_SetString(PyExc_ValueError, "producer handle is not valid");
        return NULL;
    }

    if (NULL == session) {
        DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_Factory: error [invalid session object]"));
        PyErr_SetString(PyExc_ValueError, "session object is not valid");
        return NULL;
    }

    if (!HANDLE_IS_VALID(session->session)) {
        DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_Factory: error [invalid session handle]"));
        PyErr_SetString(PyExc_ValueError, "session handle is not valid");
        return NULL;
    }

    if (NULL == (prod = (mqcrt_MQProducer *)mqcrt_MQProducer_new(&mqcrt_MQProducerType, NULL, NULL))) {
        DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_Factory: error [allocating new producer object]"));
        return NULL;
    }

    COPY_HANDLE(prod->producer, handle);
    prod->session = (PyObject *)session;
    Py_INCREF(prod->session);
    prod->closed = 0;

    DPRINTF(("%s\n", "INFO: mqcrt_MQProducer_Factory: exit"));
    return (PyObject *)prod;
}

#endif
