/*
 * Copyright (c) 2009, Michael van der Westhuizen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    - Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    - Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MQCONNECTION_CALLBACKS_MCVDW_20090517_C
#define MQCONNECTION_CALLBACKS_MCVDW_20090517_C

#include "imq.h"

#include "mqstatus.c"
#include "util.c"
#include "mqdestination.c"
#include "mqmessage.c"
#include "mqproducer.c"
#include "mqconsumer-callbacks.c"
#include "mqconsumer.c"
#include "mqsession.c"



static void connectionExceptionListenerFunc(const MQConnectionHandle connectionHandle, MQStatus exception, connectionExceptionListenerData * callbackData)
{
    PyObject * funcArgs;
    PyObject * status;
    PyObject * ret;
    PyGILState_STATE gstate;

    if (NULL == callbackData || NULL == callbackData->callable || NULL == callbackData->connection) {
        return;
    }

    gstate = PyGILState_Ensure();

    if (!PyCallable_Check(callbackData->callable)) {
        PyGILState_Release(gstate);
        return;
    }

    if (NULL == (status = mqcrt_MQStatus_Factory(exception))) {
        PyGILState_Release(gstate);
        return;
    }

    if (NULL == (funcArgs = Py_BuildValue("(OOO)", callbackData->connection, status, callbackData->args))) {
        Py_CLEAR(status);
        PyGILState_Release(gstate);
        return;
    }

    Py_INCREF(callbackData->connection);
    Py_INCREF(callbackData->callable);

    ret = PyObject_CallObject(callbackData->callable, funcArgs);

    Py_DECREF(callbackData->callable);
    Py_DECREF(callbackData->connection);

    Py_CLEAR(ret);
    Py_CLEAR(funcArgs);
    Py_CLEAR(status);
    PyGILState_Release(gstate);
}

#endif
