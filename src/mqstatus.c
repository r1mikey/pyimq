/*
 * Copyright (c) 2009, Michael van der Westhuizen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    - Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    - Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef IMQ_MQSTATUS_C_MCVDW_20090517
#define IMQ_MQSTATUS_C_MCVDW_20090517

#include "imq.h"



static PyObject * mqcrt_MQStatus_is_error(PyObject * self, void * closure)
{
    mqcrt_MQStatus * obj = (mqcrt_MQStatus *)self;

    if (MQ_TRUE == MQStatusIsError(obj->status)) {
        Py_RETURN_TRUE;
    }

    Py_RETURN_FALSE;
}


static PyObject * mqcrt_MQStatus_code(PyObject * self, void * closure)
{
    mqcrt_MQStatus * obj = (mqcrt_MQStatus *)self;
    return Py_BuildValue("i", MQGetStatusCode(obj->status));
}


static PyObject * mqcrt_MQStatus_string(PyObject * self)
{
    MQString str;
    PyObject * ret;
    mqcrt_MQStatus * obj = (mqcrt_MQStatus *)self;

    if (NULL == (str = MQGetStatusString(obj->status))) {
        return PyErr_NoMemory();
    }

    ret = PyUnicode_DecodeUTF8(str, strlen(str), "strict");
    MQFreeString(str);
    return ret;
}


static PyObject * mqcrt_MQStatus_string_c(PyObject * self, void * closure)
{
    return mqcrt_MQStatus_string(self);
}



static PyGetSetDef mqcrt_MQStatus_getseters[] = {
    {"is_error", mqcrt_MQStatus_is_error, NULL, "True if the status reqresents an error status.", NULL},
    {"code", mqcrt_MQStatus_code, NULL, "The numeric code representing this error. Zero (MQ_SUCCESS) if is_error is False.", NULL},
    {"text", mqcrt_MQStatus_string_c, NULL, "The message text corresponding to this status code.", NULL},
    {NULL}
};



static PyTypeObject mqcrt_MQStatusType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "imq.MQStatus",                         /* tp_name */
    sizeof(mqcrt_MQStatus),                 /* tp_basicsize */
    0,                                      /* tp_itemsize */
    0,                                      /* tp_dealloc */
    0,                                      /* tp_print */
    0,                                      /* tp_getattr */
    0,                                      /* tp_setattr */
    0,                                      /* tp_compare */
    0,                                      /* tp_repr */
    0,                                      /* tp_as_number */
    0,                                      /* tp_as_sequence */
    0,                                      /* tp_as_mapping */
    0,                                      /* tp_hash */
    0,                                      /* tp_call */
    mqcrt_MQStatus_string,                  /* tp_str */
    0,                                      /* tp_getattro */
    0,                                      /* tp_setattro */
    0,                                      /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,                     /* tp_flags */
    "MQStatus objects",                     /* tp_doc */
    0,                                      /* tp_traverse */
    0,                                      /* tp_clear */
    0,                                      /* tp_richcompare */
    0,                                      /* tp_weaklistoffset */
    0,                                      /* tp_iter */
    0,                                      /* tp_iternext */
    0,                                      /* tp_methods */
    0,                                      /* tp_members */
    mqcrt_MQStatus_getseters,               /* tp_getset */
};



static PyObject * mqcrt_MQStatus_Factory(MQStatus status)
{
    mqcrt_MQStatus * obj;

    if (NULL == (obj = (mqcrt_MQStatus *)mqcrt_MQStatusType.tp_alloc(&mqcrt_MQStatusType, 0))) {
        return NULL;
    }

    memset(&obj->status, 0, sizeof(obj->status));
    obj->status.errorCode = status.errorCode;

    return (PyObject *)obj;
}

#endif
