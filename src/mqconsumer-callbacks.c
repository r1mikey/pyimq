/*
 * Copyright (c) 2009, Michael van der Westhuizen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    - Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    - Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef MQCONSUMER_CALLBACKS_MCVDW_20090517_C
#define MQCONSUMER_CALLBACKS_MCVDW_20090517_C

#include "imq.h"

#include "mqstatus.c"
#include "util.c"
#include "mqdestination.c"
#include "mqmessage.c"
#include "mqproducer.c"



static MQError messageListenerFunc(const MQSessionHandle sessionHandle, const MQConsumerHandle consumerHandle, MQMessageHandle messageHandle, messageListenerData * callbackData)
{
    mqcrt_MQMessage * msg;
    MQError returnCode;
    PyObject * funcArgs;
    PyObject * ret;
    PyGILState_STATE gstate;
    mqcrt_MQSession * session;
    mqcrt_MQConsumer * consumer;
    PyObject * callable;
    PyObject * args;

    if (!HANDLE_IS_VALID(sessionHandle) || !HANDLE_IS_VALID(consumerHandle) || !HANDLE_IS_VALID(messageHandle) || NULL == callbackData) {
        return MQ_CALLBACK_RUNTIME_ERROR;
    }

    if (NULL == callbackData || NULL == callbackData->consumer || NULL == callbackData->callable) {
        return MQ_CALLBACK_RUNTIME_ERROR;
    }

    callable = callbackData->callable;
    args = callbackData->args;
    consumer = (mqcrt_MQConsumer *)callbackData->consumer;

    if (NULL == consumer->session) {
        return MQ_CALLBACK_RUNTIME_ERROR;
    }

    session = (mqcrt_MQSession *)consumer->session;

    if (consumerHandle.handle != consumer->consumer.handle) {
        return MQ_CALLBACK_RUNTIME_ERROR;
    }

    if (sessionHandle.handle != session->session.handle) {
        return MQ_CALLBACK_RUNTIME_ERROR;
    }

    gstate = PyGILState_Ensure();

    if (!PyCallable_Check(callable)) {
        PyGILState_Release(gstate);
        return MQ_CALLBACK_RUNTIME_ERROR;
    }

    if (NULL == (msg = (mqcrt_MQMessage *)PyObject_CallObject((PyObject *)&mqcrt_MQMessageType, NULL))) {
        PyGILState_Release(gstate);
        return MQ_CALLBACK_RUNTIME_ERROR;
    }

    msg->message.handle = messageHandle.handle;
    msg->session = (PyObject *)session;
    Py_XINCREF(msg->session);

    if (NULL == args || Py_None == args) {
        funcArgs = Py_BuildValue("(OOO)", session, consumer, msg);
    } else {
        funcArgs = Py_BuildValue("(OOOO)", session, consumer, msg, callbackData->args);
    }

    if (NULL == funcArgs) {
        INIT_HANDLE(msg->message);
        Py_CLEAR(msg);
        PyGILState_Release(gstate);
        return MQ_CALLBACK_RUNTIME_ERROR;
    }

    Py_XINCREF(consumer);

    returnCode = MQ_SUCCESS;
    ret = PyObject_CallObject(callbackData->callable, funcArgs);

    if (NULL != PyErr_Occurred()) {
        returnCode = MQ_CALLBACK_RUNTIME_ERROR;
        PyErr_Print();
        INIT_HANDLE(msg->message);
    }

    Py_CLEAR(funcArgs);
    Py_CLEAR(msg);
    Py_CLEAR(ret);
    Py_CLEAR(consumer);

    PyGILState_Release(gstate);
    return returnCode;
}


#if 0
static MQError messageListenerBAFunc(const MQSessionHandle sessionHandle, const MQConsumerHandle consumerHandle, const MQMessageHandle messageHandle, MQError errorCode, void * callbackData)
{
    return MQ_SUCCESS;
}
#endif

#endif
