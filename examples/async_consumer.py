#!/usr/bin/env python

import os

if not 'MQ_LOG_LEVEL' in os.environ:
    os.environ['MQ_LOG_LEVEL'] = '-1'

from optparse import OptionParser
import sys, time
#sys.path.insert(0, 'build/lib.macosx-10.8-intel-2.7')
from imq import MQConnection, MQMessage
from imq import MQ_BROKER_HOST_PROPERTY, MQ_BROKER_PORT_PROPERTY, MQ_CONNECTION_TYPE_PROPERTY, \
    MQ_CLIENT_ACKNOWLEDGE, MQ_SESSION_ASYNC_RECEIVE, MQ_QUEUE_DESTINATION, MQ_TOPIC_DESTINATION, \
    MQ_TEXT_MESSAGE, MQ_MESSAGE_TYPE_HEADER_PROPERTY, MQ_REDELIVERED_HEADER_PROPERTY, \
    MQ_STRING_TYPE, MQ_INT32_TYPE

g_exit_flag = False


def listener(session, consumer, message): # you could add data=None if you passed callback data
    if message.type() != MQ_TEXT_MESSAGE:
        sys.stdout.write("Received mesage is not MQ_TEXT_MESSAGE type.\n")
        sys.stdout.flush()
        message.acknowledge_messages()
        return

    text = message.get_text()
    sys.stdout.write("Received message: " + text + "\n")
    sys.stdout.flush()

    if 'END' == text:
        sys.stdout.write("Exit requested\n")
        sys.stdout.flush()
        global g_exit_flag
        g_exit_flag = True

    properties = message.get_properties()

    if 'index' in properties:
        sys.stdout.write('\tProperty index=%d\n' % properties["index"][1])
        sys.stdout.flush()

    headers = message.get_headers()
    sys.stdout.write("\tHeader redelivered=%s\n" % headers[MQ_REDELIVERED_HEADER_PROPERTY][1])
    sys.stdout.flush()

    if MQ_MESSAGE_TYPE_HEADER_PROPERTY in headers:
        sys.stdout.write("\tHeader message-type=%s\n" % headers[MQ_MESSAGE_TYPE_HEADER_PROPERTY][1])
        sys.stdout.flush()

    message.acknowledge_messages()



def async_consumer(host, port, name, type):
    conn = MQConnection({MQ_BROKER_HOST_PROPERTY: (MQ_STRING_TYPE, host),
            MQ_BROKER_PORT_PROPERTY: (MQ_INT32_TYPE, port),
            MQ_CONNECTION_TYPE_PROPERTY: (MQ_STRING_TYPE, "TCP")}, "guest", "guest")
    sess = conn.create_session(False, MQ_CLIENT_ACKNOWLEDGE, MQ_SESSION_ASYNC_RECEIVE)
    cons = sess.create_async_consumer(sess.create_destination(name, type), None, False, listener)
    conn.start()

    while not g_exit_flag:
        time.sleep(0.2)



if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option("--host", action="store", dest="host", default="localhost")
    parser.add_option("--port", action="store", dest="port", default=7676, type="int")
    parser.add_option("--destination_name", action="store", dest="destination_name", default="example_producerconsumer_dest")
    parser.add_option("--type", action="store", dest="type", choices=("q", "t",), default="q")
    (options, args) = parser.parse_args()

    args = {'host' : options.host, 'port' : options.port, 'name' : options.destination_name,
        'type' : 't' == options.type and MQ_TOPIC_DESTINATION or MQ_QUEUE_DESTINATION}

    async_consumer(**args)
