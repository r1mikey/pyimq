################################
PyIMQ: Python bindings to OpenMQ
################################

**This documentation is a work-in-progress, and will be updated frequently**

*pyimq* is a set of Python_ bindings to `Open Message Queue`_ (also known as the Glassfish Message Queue, formerly known as the Sun Java System Message Queue and informally known as IMQ).

This software wraps the `Sun GlassFish Message Queue C runtime library`_ in Python_, modelling the implied object oriented design of the C interface.

The most common message queue usage patterns are supported, including asynchronous message consumers.

********
Contents
********

.. toctree::
   :maxdepth: 2

   gettingstarted

***************
A Quick Example
***************

This example shows a simple consumer, which simply retrieves messages and prints them.

.. literalinclude:: ../examples/consume_forever.py

******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`search`

.. _Python: http://www.python.org/
.. _`Open Message Queue`: http://mq.java.net/
.. _`Sun GlassFish Message Queue C runtime library`: http://docs.oracle.com/cd/E19879-01/821-0030/index.html
