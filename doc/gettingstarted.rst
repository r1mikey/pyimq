Getting Started
===============

If you are already familiar with the OpenMQ C runtime library you should still read this section to familiarise yourself with the mqcrt object hierarchy as exposed to Python.

If you are not familiar with the OpenMQ C runtime library you may want to open a copy of the `mqcrt documentation`_ in another tab.

*************
Prerequisites
*************

To use the *pyimq* module you'll need access to an OpenMQ installation (or Sun Java System Message Queue, or Glassfish Message Queue).  If you do not yet have access to such a beast, this document will help you to get a simple OpenMQ instance up and running.

You'll also need to OpenMQ C runtime library, as the pyimq module uses this C library to access the message queue broker.

The OpenMQ C runtime library (mqcrt) is distributed along with platform specific builds of OpenMQ, but is often not quite what you really need to build a Python extension module against, so if you need to rebuild mqcrt you'll need a C++ compiler and some patience.

Finally, if you're running the OpenMQ broker (server) on your machine you'll need a Java_ installation.  If you plan to build mqcrt you'll also need a Java_ JDK and Ant_.

****************
Obtaining OpenMQ
****************

*notes on downloading and running an instance of OpenMQ*
 1) Download...
 2) Unzip...
 3) Run...

******************************************
Building the Message Queue Runtime Library
******************************************

*notes on building, when you need to, and where to find more information*

*prereqs*

 1) Extract
 2) Patch
 3) Configure
 4) Build
 5) Install

**************
Running OpenMQ
**************

*a quick introduction to running an instance*

******************
Writing a Producer
******************

*how to write a producer*

******************
Writing a Consumer
******************

*how to write a consumer*

.. _`mqcrt documentation`: http://docs.oracle.com/cd/E19879-01/821-0030/index.html
.. _Java: http://www.oracle.com/technetwork/java/javase/downloads/index.html
.. _Ant: http://ant.apache.org
